<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Session</title>
</head>
<body>
	<form method="POST">
    	First number <input type = "text" name="firstnum">
    	Second number <input type = "text" name="secondnum">
    	<button name ="button" value ="submit" type="submit">Calculate</button> 
    	
    </form>
 <%
 	double result=0;
    double a = 0;
    double b = 0;
    try{
    	a = Double.parseDouble(request.getParameter("firstnum"));
    	b = Double.parseDouble(request.getParameter("secondnum"));
    }
    catch(NullPointerException e){ return; }
    catch(NumberFormatException e){
    	out.println("Numbers are not entered");
    	return;
    }
    result = a+b;
    session.setAttribute("result",result);
    %>
    <jsp:forward page="Session2.jsp" />
</body>
</html>