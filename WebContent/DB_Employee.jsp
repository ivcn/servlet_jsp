<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.DriverManager"
		import="java.sql.Statement"
		import="java.sql.Connection"
		import="java.sql.ResultSet"
		import="java.sql.SQLException"
%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form method="POST">
		Number <input type = "text" name = "EmpNum"><br>
		Employee Name <input type="text" name="EmpName"><br>
		Job <input type = "text" name ="JobTitle"><br>
		<button name = "button" type="submit">Send</button><br>
	</form>
	
	<%
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		int EmpNum=0;
		String EmpName="";
		String JobTitle="";
		try{
			EmpNum = Integer.parseInt(request.getParameter("EmpNum"));
			EmpName = request.getParameter("EmpName");
			JobTitle = request.getParameter("JobTitle");
			
		}
		catch(NumberFormatException e){ }
		
		try{
			conn = DriverManager.getConnection("jdbc:derby://localhost:1527/Lesson22");
			String query = "INSERT INTO Employee VALUES("+EmpNum+","+"'"+EmpName+"'"+","+"'"+JobTitle+"'"+")";
			stmt = conn.createStatement();
			if((EmpName != "") && (JobTitle != "")&&(EmpNum!=0) )
					stmt.execute(query);
			
			rs = stmt.executeQuery("SELECT * FROM Employee");
			while (rs.next()){
				int empNo = rs.getInt("EMPNO");
				String eName = rs.getString("ENAME");
				String job = rs.getString("JOB_TITLE");	
				out.println(""+empNo + "," + eName + ","  + job+"<br>");
			}
		}
		catch(SQLException e){
			System.out.println("SQL Error"+e.getMessage()+"code:"+e.getErrorCode());
		}
		catch(Exception e){
			out.println(e.getMessage());
			e.printStackTrace();
		}
		finally{
			try{
				rs.close();
				stmt.close();
				conn.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	%>

</body>
</html>